import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisException;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.junit.*;
import org.testcontainers.containers.GenericContainer;


/**
 * The Naming pattern used in this project conforms to the
 *  [UnitOfWork_StateUnderTest_ExpectedBehavior] Standard.
 *
 *  I know that i could have statically imported the Assert class but i preferred to write it manually to get the hang of it.
 */
public class RedisListTests {
    private RedisClient redisClient = RedisClient.create("redis://localhost:6379");
    private StatefulRedisConnection<String, String> connection = redisClient.connect();
    private RedisCommands<String, String> cmd = connection.sync();

    @ClassRule
    public static GenericContainer redis =
            new GenericContainer("redis:4.0.2-alpine")
                    .withExposedPorts(6379);
    @Before
    public void setup() {

        redisClient = RedisClient.create("redis://"  + redis.getContainerIpAddress() + ":" + redis.getMappedPort(6379).toString());
        connection = redisClient.connect();
        cmd = connection.sync();
        cmd.del("TestList");
        System.out.println("Connection established");
    }

    @Before
    public void start() {
        redisClient = RedisClient.create("redis://localhost:6379");
        connection = redisClient.connect();
        cmd = connection.sync();
        cmd.del("TestList");
        System.out.println("Connection established");
    }

    @Test
    public void rpush_ProperOrder_True() {
        cmd.rpush("TestList", "a", "b", "c", "d");
        Assert.assertEquals("[a, b, c, d]", cmd.lrange("TestList", 0, 3).toString());
    }

    @Test
    public void lrem_DecreaseInLength_True() {
        cmd.lpush("TestList", "a", "b", "c", "d", "d");
        cmd.lrem("TestList", 2, "d");
        Assert.assertEquals(Long.valueOf(3L), (cmd.llen("TestList")));
    }

    @Test
    public void lpushx_PushIntoNoKeyList_True() {
        cmd.lpushx("TestListFake", "a");
        Assert.assertEquals("[]", cmd.lrange("TestListFake", 0, 0).toString());
    }

    @Test
    public void ltrim_halveList_True() {
        cmd.rpush("TestList", "a", "b", "c", "d");
        //removes second last item and the last one
        cmd.ltrim("TestList", -2, -1);
        Assert.assertEquals("[c, d]", cmd.lrange("TestList",0,1).toString());
    }

    @Test
    public void brpoplpush_EmptySource_True()
    {
        //if no value found within the 1 second timeout, it returns null.
        Assert.assertEquals(null,cmd.brpoplpush(1,"TestListFake","TestListFake2"));
    }

    @Test
    public void lpop_RemoveFirstElement_true()
    {
        cmd.rpush("TestList", "a", "b", "c", "d");
        Assert.assertEquals("a",cmd.lpop("TestList"));
    }

    @Test
    public void lset_OverwriteSecondElement_true()
    {
        cmd.rpush("TestList", "a", "b", "c", "d");
        Assert.assertEquals("OK",cmd.lset("TestList",0,"aaa"));
    }

    @After
    public void stop() {
        connection.close();
        redisClient.shutdown();
        System.out.println("Connection closed");
    }
}